android-tools
=============

* Select and download and decompress fonts from http://fontello.com

```sh
# Little helper for android glyphs to icons convertion
android_glyphs_to_icons() { fontdir=$1; dpi=$2; theme=$3
  case $theme in
    holo-dark ) color='#ffffffcc'                ;;
    holo-light) color='#33333399'                ;;
    *         ) echo "unsuported theme" ; exit 1 ;;
  esac
  glyphs-to-icons --indir "${fontdir}" --outdir "${theme}/drawable-${dpi}" \
                  --size  "${dpi}"     --color  "${color}"                 \
                  --prefix icon-       --file-mangling
}

# Build icons for various themes and resolutions
for theme in holo-dark holo-light ; do
  for dpi in mdpi hdpi xhdpi xxhdpi ; do
    android_glyphs_to_icons ${FONTDIR} ${dpi} ${theme}
  done
done
```
