#!/usr/bin/env ruby
# -*- coding: utf-8 -*-

#
# AUTHOR : Stephane D'Alu
# LICENSE: GPLv2
#

#
# Select the glyphs that you need by using:
#  http://fontello.com/
#

# Android holo light: #33333399   (grey20 60%)
# Android holo dark:  #ffffffcc   (white  80%)

require 'optparse'
require 'rmagick'
require 'json'
require 'fileutils'


CFG = {
    :fontdir   		=> '.',
    :fonttype  		=> nil,
    :outdir    		=> 'out',
    :prefix    		=> nil,
    :file_mangling	=> nil,
    :size      		=> :xxhdpi,
    :pointsize 		=> nil,
    :overflow  		=> :none,
    :offset_x  		=> nil,
    :offset_y  		=> nil,
    :color     		=> 'black',
}


SIZES = {
    :xxhdpi => 96,
    :xhdpi  => 64,
    :hdpi   => 48,
    :mdpi   => 32,
}


optparse = OptionParser.new do|opts|
    opts.banner = "Usage: #{opts.program_name} [options] FONTDIR"

    opts.separator ''
    opts.separator 'Convert font glyph to icon.'
    opts.separator '(Support conversion directly from Fontello or FontAwesome)'
    opts.separator ''
    
    opts.on '-o', '--outdir DIR', 'Icon directory' do |d|
        CFG[:outdir   ] = d
    end
    opts.on '-t', '--type NAME', 'Font type' do |n|
        CFG[:fonttype] = n
    end
    opts.on       '--prefix PREFIX', 'Prefix icon name' do |p|
        CFG[:prefix   ] = p
    end
    opts.on       '--file-mangling', 'File mangling' do
        CFG[:file_mangling] = true
    end
    opts.on       '-s', '--size SIZE', 'Icon size' do |s|
        CFG[:size     ] = s
    end
    opts.on '-p', '--pointsize SIZE', Float, 'Font point size' do |s|
        CFG[:pointsize] = s
    end
    opts.on       '--overflow METHOD', [:ok, :none], 'Dealing with overflow' do |o|
        CFG[:overflow ] = o
    end
    opts.on       '--offset-x X', Float, 'Offset by x% of size' do |o|
        CFG[:offset_x ] = o / 100.0
    end
    opts.on       '--offset-y Y', Float, 'Offset by y% of size' do |o|
        CFG[:offset_y ] = o / 100.0
    end
    opts.on '-c', '--color COLOR',       'Icon color' do |c|
        CFG[:color    ] = c
    end
    opts.on '-h', '--help',              'Display this screen' do
        puts opts
        exit
    end
end
optparse.parse!

if ARGV.size != 1
    puts optparse
    exit
end

CFG[:fontdir  ] = ARGV[0]


class IconMaker
    def initialize(size, color)
        self.size     = size
        self.color    = color
        self.offset_x = 0
        self.offset_y = 0
    end

    def size=(s)
        @size      = case s
                     when String then SIZES[s.to_sym] || Integer(s)
                     when Symbol then SIZES[s]
                     else             s
                     end
        @pointsize = @size
        raise "invalid size '#{@size}'" if @size.nil? || @size < 8
    end

    def pointsize=(s) ; @pointsize = s ; end
    def offset_x=(x)  ; @offset_x  = x ; end
    def offset_y=(y)  ; @offset_y  = y ; end
    def color=(c)     ; @color     = c ; end

    def boxing(overflow = :none)
        return if overflow == :ok

        draw = Magick::Draw.new
        draw.gravity     = Magick::CenterGravity
        draw.pointsize   = @pointsize
        draw.fill        = @color
        draw.font        = @font

        height, width    = 0, 0
        widths           = []
        @glyphs.each {|name, char|
            metrics = draw.get_type_metrics char
            h, w = metrics.height, metrics.width
            height  = h if height < h
            width   = w if width  < w
            widths << w if w > @size
        }

        pointsize = case overflow
                    when :none
                        @pointsize * @size  / [height,width].max
                    else
                        raise "unknown overflow method '#{overflow}'"
                    end
        if pointsize < @pointsize
            warn "Reducing pointsize: #{@pointsize} => #{pointsize.to_i}"
            @pointsize = pointsize
        end
    end


    def source_fontello(dir, **opts)
        descfile = File.join(dir, 'config.json')
        return false unless File.exist?(descfile)
        
        cfg      = JSON.parse(File.read(descfile))
        @font    = File.join(dir, 'font', 'fontello.ttf')
        @glyphs  = Hash[ cfg['glyphs'].map {|g|
                             [ g['css'], [ g['code']].pack('U') ] } ]
        true
    end

    def source_fontawesome(dir, variant: nil, **opts) # Font Awesome >= 5.x
        descfile = File.join(dir, 'less', '_variables.less')
        return false unless File.exist?(descfile)

        fname = case variant
                when nil, 'regular' then 'fa-regular-400.ttf'
                when 'light'        then 'fa-light-300.ttf'
                when 'solid'        then 'fa-solid-900.ttf'
                when 'brand'        then 'fa-brand-400.ttf'
                else raise "unsupported variant '#{variant}'"
                end
        
        @font   = File.join(dir, 'webfonts', fname)
        @glyphs = Hash[File.readlines(descfile)
                           .map {|l| if l =~ /fa-var-([^:]+):
                                              \s*"\\([a-fA-F0-9]+)"\s*;/x
                                         [ $1, [$2.hex].pack('U') ]
                                     end }
                           .compact]
        @offset_x = 0.04
    end

    def source(dir, **opts)
        unless Dir.exist?(dir)
            raise "font source directory (#{dir}) doesn't exist"
        end
        source_fontello(dir, **opts) || source_fontawesome(dir, **opts)
    end

    def generate(dir, opts={})
        FileUtils.mkdir_p(dir)

        draw = Magick::Draw.new
        draw.gravity     = Magick::CenterGravity
        draw.pointsize   = @pointsize
        draw.fill        = @color
        draw.font        = @font

        x = @offset_x * @size
        y = @offset_y * @size

        @glyphs.each {|name, char|
            fname = name.dup
            fname = opts[:prefix] + fname if opts[:prefix]
            fname.gsub!(/[^a-z0-9_]/i, '_') if opts[:file_mangling]
            icon  = Magick::Image.new(@size, @size) {
                self.background_color = "transparent"
            }
            metrics = draw.get_type_metrics char
            h, w = metrics.height, metrics.width
            warn "glyph '#{name}' is overflowing" if w > @size
            draw.annotate(icon, 0, 0, x, y, char)
            if icon.to_blob {|img| img.format = 'rgba'; img.depth = 8 }
                   .each_byte.each_cons(4).uniq.size == 1
                warn "no glyph '#{name}'"
                next
            end
            icon.write("#{dir}/#{fname}.png")
        }
    end
end



im = IconMaker.new(CFG[:size], CFG[:color])
im.source(CFG[:fontdir], variant: CFG[:fonttype])
im.offset_x = CFG[:offset_x] if CFG[:offset_x]
im.offset_y = CFG[:offset_y] if CFG[:offset_y]

if CFG[:pointsize]
then im.pointsize = CFG[:pointsize]
else im.boxing(CFG[:overflow])
end

im.generate(CFG[:outdir],
            :prefix => CFG[:prefix], :file_mangling => CFG[:file_mangling])
